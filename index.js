console.log("Array - Vetor - Lista") 

let frutas = ['maça','banana','maracujá']
console.log(frutas[2]) //mostrar apenas um elemento do array 

frutas[3] = 'abacaxi'
frutas.push('melancia') //adiciona o valor no final do Array
frutas.unshift('melão') //adiciona o valor no início do Array
frutas.pop() //remove o último valor do Array
frutas.shift() //remove o primeiro valor do Array

console.log(frutas) //apresentar o array por completo
console.log(frutas.slice(2,5)) //apresentar uma fatia do vetor
frutas.splice(1,3) //remove uma fatia de valores do


console.log(frutas)
console.log(frutas.indexOf('maça')) //retorna o índice do valor procurado
console.log(frutas.length) //quantidade de elementos no Array

for (let i=0;i<frutas.length;i++){
    console.log(frutas[i])
}

for (let fruta of frutas){
    console.log(fruta+':'+frutas.indexOf(fruta))
} 

function maiorDoVetor(vetor){
    let maior = vetor[0]
    for (let n of vetor){
        if(n > maior){
            maior = n
        }
    }
    return maior
    }

    //Ex1. Crie um vetor com os números 7,4,8,2,9
    let números = [7,4,8,2,9]
    console.log(números)
    console.log("")

    //Ex2. Adicione o número 5 no final do vetor
    números.push(5)
    console.log(números)
    console.log("")

    //EX3. Adicione o número 1 no início do vetor
    números.unshift(1)
    console.log(números)
    console.log("")

    //Ex4. Remova o terceiro ao quinto elemento
    números.splice(2,3)
    console.log(números)
    console.log("")

    //Ex5. Mostre quantos elementos tem no vetor
    console.log(números.length)
    console.log("")

    //Ex6. Mostre em qual índice está o número 7
    console.log(números.indexOf(7))
    console.log("")